-----------------------------------------------------------------------------------------------------------------------
--
-- Hystory tables which don't have
--    Start_Date (Begin_Date) or 
--    End_Date (Stop_Date) fields
--
-----------------------------------------------------------------------------------------------------------------------

  select t.Table_Name
    from User_Tables t
   where t.Table_Name like '%HISTOR%' 
     and (not exists (select 1
                        from user_tab_columns tc
                       where tc.Table_Name = t.Table_Name 
                         and (tc.Column_Name like 'START%DATE' or
                              tc.Column_Name like 'BEGIN%DATE'))
                           
           or    
           
          not exists (select 1
                        from user_tab_columns tc
                       where tc.Table_Name = t.Table_Name 
                         and (tc.Column_Name like 'END%DATE' or
                              tc.Column_Name like 'STOP%DATE')))
order by t.Table_Name    