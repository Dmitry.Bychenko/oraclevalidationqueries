-----------------------------------------------------------------------------------------------------------------------
--
-- Tables that don't have neither primary key nor unique key
--
-----------------------------------------------------------------------------------------------------------------------

   select t.Table_Name
     from User_Tables t
    where t.Table_Name not in (select c.Table_Name
                                 from User_Constraints c
                                where c.Constraint_Type in ('P', 'U') and
                                      c.Status = 'ENABLED' and
                                      c.Deferred = 'IMMEDIATE')
 order by t.Table_Name   