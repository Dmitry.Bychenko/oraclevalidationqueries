-----------------------------------------------------------------------------------------------------------------------
--
-- Check (return counter examples)
-- If Primary Key field ends with "ID", 
-- the corresponding Foreign Ley field is Ends with "PrimaryKeyField"
-- E.g. My_Id <-> Some_Prefix_My_Id
--
-----------------------------------------------------------------------------------------------------------------------

   select fkc.Table_Name     as FK_Table_Name,
          fkc.Column_Name    as FK_Column_Name,
          pkc.Table_Name     as PK_Table_Name,
          pkc.Column_Name    as PK_Column_Name,
          fk.Constraint_Name as FK_Constraint_Name,
          pk.Constraint_Name as PK_Constraint_Name
     from User_Cons_Columns fkc join
          User_Constraints  fk on (fk.Constraint_Name = fkc.Constraint_Name and 
                                   fk.Constraint_Type = 'R' and
                                   fk.Status = 'ENABLED') join 
          User_Constraints  pk on (pk.Constraint_Name = fk.R_Constraint_Name and 
                                   pk.Constraint_Type = 'P' and
                                   pk.Status = 'ENABLED') join 
          User_Cons_Columns pkc on (pk.Constraint_Name = pkc.Constraint_Name and
                                    pkc.Position = fkc.Position)
   where pkc.Column_Name like '%ID' 
     and fkc.Column_Name not like '%' || pkc.Column_Name
order by fkc.Table_Name,
         fkc.Column_Name 